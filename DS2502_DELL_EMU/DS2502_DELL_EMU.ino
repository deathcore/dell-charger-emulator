#include "OneWireHub.h"
#include "DS2502.h"

constexpr uint8_t pin_onewire   { 2 }; // Set pin2 as Dell cable center pin

constexpr uint8_t charger130W[4] = {0x31, 0x33, 0x30};  //130W second digit of each hex-number
constexpr uint8_t charger090W[4] = {0x30, 0x39, 0x30};  //90W
constexpr uint8_t charger065W[4] = {0x30, 0x36, 0x36};  //66W
constexpr uint8_t charger060W[4] = {0x30, 0x36, 0x30};  //60W - USB C ZYPDS @ 20V (Tested with RAVPower Portable Charger 20000mAh 60W PD Power Bank)
constexpr uint8_t charger045W[4] = {0x30, 0x34, 0x35};  //45W - USB C ZYPDS @ 20V (Tested with RAVPower Portable Charger 20000mAh 60W PD Power Bank)
constexpr uint8_t charger045W[4] = {0x30, 0x31, 0x38};  //18W

auto hub       = OneWireHub(pin_onewire);
auto dellCH    = DS2502( 0x28, 0x0D, 0x01, 0x08, 0x0B, 0x02, 0x0A); // Skip rom and listen to all devices

void setup()
{
    hub.attach(dellCH);
    dellCH.writeMemory(charger090W, sizeof(charger090W), 0x08);
}

void loop()
{
    hub.poll();
}