# Dell Charger Emulator

Dell Charger handshake emulator optimised for Digispark with various wattage modes.


**Charger pinout :**

| Contact | Detail |
| ------ | ------ |
| Outer Barrel | 19.5V |
| Inner Barrel | GND |
| Center Pin | Charger Detect (Pin2 on DigiSpark) | 
